#ifndef PSYS_BASE_SHELL_H
#define PSYS_BASE_SHELL_H

typedef struct params {
	char *args[255];
	int arg_length[255];
	int argcount;
} params_t;

typedef struct command_def {
	char *name;
	int (*fct)(void *);
} command_def_t;

typedef struct command {
	char *name;
	int (*fct)(void *);
	params_t params;
} command_t;

int noui_shell_start(void *arg);

#endif //PSYS_BASE_SHELL_H
