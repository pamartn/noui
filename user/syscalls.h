#ifndef PSYS_BASE_NOUILIB_H
#define PSYS_BASE_NOUILIB_H

#include "../shared/stddef.h"
#include "../shared/stdint.h"

extern void console_putbytes(const char *chaine, int taille);

extern void cons_write(const char *chaine, int taille);

extern unsigned long cons_read(char *string, unsigned long length);

extern void exit_p(int retval);

extern int start(int (*pt_func)(void*), unsigned long ssize, int prio, const char *name, void *arg);

extern int waitpid(int pid, int *retvalp);

extern int kill(int pid);

extern int getpid(void);

extern int getprio(int pid);

extern int chprio(int pid, int newprio);

extern int pcreate(int count);

extern int psend(int fid, int message);

extern int pdelete(int fid);

extern int pcount(int fid, int *count);

extern int preceive(int fid,int *message);

extern int preset(int fid);

extern void clock_settings(unsigned long *quartz, unsigned long *ticks);

extern void wait_clock(unsigned long clock);

extern unsigned long current_clock(void);

extern void cons_echo(int on);

// Custom

extern void get_hour_str(char *str, int length);

extern void top();

extern void ps();

extern void write_char(uint32_t line, uint32_t column, char c, uint32_t text_color, uint32_t background_color);

extern void clear_screen(void);
			
extern void get_key(int * key);

extern void msgstate(void);

extern void beep(uint32_t frequency, uint32_t duration);

#endif //PSYS_BASE_NOUILIB_H
