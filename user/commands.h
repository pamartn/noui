#ifndef _COMMANDS_H_
#define _COMMANDS_H_

#include "noui_shell.h"

int command_ps(void *args);
int command_top(void *args);
int command_print(void *args);
int command_msgstate(void *args);
int command_test(void *args);

unsigned get_command_count();

#endif
