#include "commands.h"
#include "syscalls.h"
#include "tetris.h"
#include "../shared/stdio.h"
#include "test.h"

command_def_t commands[] = {
        {"top",    command_top},
        {"ps",     command_ps},
        {"print",  command_print},
        {"tetris", command_tetris},
        {"msgstate", command_msgstate},
        {"test", command_test}
};

unsigned get_command_count() {
    return sizeof(commands);
}


int command_ps(void *args) {
    (void) args;
    ps();
    return 0;
}

int command_top(void *args) {
    (void) args;
    top();
    return 0;
}

int command_print(void *args) {
    params_t *p = (params_t *) args;
    for (int i = 0; i < p->argcount; i++) {
        printf("%s", p->args[i]);
        if (i < p->argcount - 1)
            printf(" ");
    }
    printf("\n");
    return 0;
}

int command_msgstate(void *args) {
    (void) args;
    msgstate();
    return 0;
}

int command_test(void *args) {
    (void) args;
    int pid = start(test_proc, 4096, 128, "test", NULL);
    waitpid(pid, NULL);
    return 0;
}