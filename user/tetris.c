#include "tetris.h"
#include "../shared/stdio.h"
#include "../shared/string.h"
#include "mem.h"
#include "syscalls.h"

#define for_each_part(piece) for(unsigned i = 0; i < sizeof(piece->parts)/sizeof(position_t); i++)

char banner[] = "ooooooooooooo oooooooooooo ooooooooooooo ooooooooo.   ooooo  .oooooo..o  \n"
"8'   888   `8 `888'     `8 8'   888   `8 `888   `Y88. `888' d8P'    `Y8  \n"
"     888       888              888       888   .d88'  888  Y88bo.       \n"
"     888       888oooo8         888       888ooo88P'   888   `\"Y8888o.   \n" "     888       888     \"         888       888`88b.     888       `\"Y88b \n"
"     888       888       o      888       888  `88b.   888  oo     .d8P  \n"
"    o888o     o888ooooood8     o888o     o888o  o888o o888o 8""88888P'     \n\n\n" \
"                    ***** Press any key to play *****                   ";

typedef struct position {
	int x;
	int y;
} position_t;

typedef struct game_piece {
	position_t parts[4];	
	position_t anchor;
} game_piece_t;

#define SCREEN_WIDTH 80
#define SCREEN_HEIGHT 25

#define GAME_HEIGHT 20
#define GAME_WIDTH 10

int game_buffer[GAME_HEIGHT][GAME_WIDTH];
int loose = 0;
game_piece_t *current_piece = NULL;
unsigned long last_time = 0;
unsigned long speed = 5;
position_t game_position;
int score = 0;
int quit = 0;

void display_banner() {
	int banner_size = sizeof(banner);
	int lig = 0;
	int col = 0;
	for(int i = 0; i < banner_size; i++)
	{
		switch(banner[i]) {
			case '\n':
				lig++;
				col = 0;
				break;
			default:
				write_char(lig, col, banner[i], 0, 12);
				col++;
				break;
		}
	}
}

void display_loose_screen() {
	char buffer[60];
	int length = snprintf(buffer, 60, "You loose !Score : %d", score);
	for(int i = 0; i < length; i++)
		write_char(0, i, buffer[i], 0, 10);

	cons_read(buffer, 1);
}

void clear_screen_color(int color) {
	for(int x = 0; x < SCREEN_WIDTH; x++)
		for(int y = 0; y < SCREEN_HEIGHT; y++)
			write_char(y, x, ' ', 0, color);
}

void blink_screen() {
	int clock = 5;
	clear_screen_color(13);
	wait_clock(current_clock()+clock);
	clear_screen();
	wait_clock(current_clock()+clock);
	clear_screen_color(13);
	wait_clock(current_clock()+clock);
	clear_screen();
	wait_clock(current_clock()+clock);
	clear_screen_color(13);
}

game_piece_t *create_piece(int id, int x, int y) {
	game_piece_t *piece = mem_alloc(sizeof(game_piece_t));
	switch(id) {
		case 1:
			for_each_part(piece)
			{
				piece->parts[i].x = x;
				piece->parts[i].y = y+i;
			}
			piece->anchor.y = y;
			piece->anchor.x = x;
			break;
	}
	return piece;
}
		
int has_loose() {
	for_each_part(current_piece)
	{
		if (game_buffer[current_piece->parts[i].y][current_piece->parts[i].x] != ' ')
		{
			loose = 1;
			return 1;
		}
	}
	return 0;
}
		
void display_game(int fid) {
	psend(fid, 1);
	char buffer[20];
	int length = snprintf(buffer, 20, "Score : %d", score);
	for(int i = 0; i < length; i++)
		write_char(0, i, buffer[i], 0, 10);

	for(int j = 0; j < GAME_WIDTH; j++)
	{
		write_char(game_position.y+GAME_HEIGHT, j+game_position.x, ' ', 0, 14);
		write_char(game_position.y-1, j+game_position.x, ' ', 0, 14);
	}

	for(int i = 0; i < GAME_HEIGHT; i++)
	{
		write_char(i, game_position.x-1, ' ', 0, 14);
		write_char(i, game_position.x+GAME_WIDTH, ' ', 0, 14);
		for(int j = 0; j < GAME_WIDTH; j++)
			if (game_buffer[i][j] == ' ')
				write_char(i, j+game_position.x, ' ', 0, 0);
			else
				write_char(i, j+game_position.x, ' ', 0, 12);
	}
	for_each_part(current_piece)
	{
		write_char(current_piece->parts[i].y, current_piece->parts[i].x+game_position.x, ' ', 0, 12);
	}
	preceive(fid, NULL);
}

/* 
 * Move the piece to the specified x & y position
 * int fid is the queue id used to simulate a lock on current_piece
 */
void move_piece(int fid, int x, int y) {
	psend(fid, 1);
	position_t old_parts[4];
	int restore = 0;
	/* Save the current piece parts
	 */
	memcpy(old_parts, current_piece->parts, 4*sizeof(position_t));
	for_each_part(current_piece)
	{
		int px = current_piece->parts[i].x;
		int py = current_piece->parts[i].y;
		px += x;
		py += y;
		if (py < 0 || py > GAME_HEIGHT-1 || px < 0 || px > GAME_WIDTH-1 || game_buffer[py][px] != ' ')
		{
			/* Here one of our part is out of game bounds, or colliding with something in game array
			 * We set this bool to 1 in order to restore the old parts of the piece
			 */
			restore = 1;
			break;
		}
		current_piece->parts[i].x = px;
		current_piece->parts[i].y = py;
	}
	if (restore)
	{
		/* Restore current piece parts
		 */
		memcpy(current_piece->parts, old_parts, 4*sizeof(position_t));
	} else {
		current_piece->anchor.y += y;
		current_piece->anchor.x += x;
	}
	preceive(fid, NULL);
}

void change_current_piece() {
	if (current_piece != NULL)
		mem_free(current_piece, sizeof(game_piece_t));

	current_piece = create_piece(1, 5, 0);
}

/*
 * Check lines around the current_piece in order to :
 * Delete line if it is complete and increment score
 *
 * NEEDS TO BE CALLED SAFELY : take a lock on current_piece
 * before calling this function
 */
void check_lines() {
	int y = -1;
	int j = 0;
	for_each_part(current_piece)
	{
		/* Just check if we haven't checked/erased this line before
		 */
		if (y != current_piece->parts[i].y)
		{
			y = current_piece->parts[i].y;
			/* Skip until finding empty space on line
			 */
			for(j = 0; j < GAME_WIDTH && game_buffer[y][j] != ' '; j++);
			if (j == GAME_WIDTH) {
				score++;
				blink_screen();
				/* The last line can't be erased by moving memory,
				 * we need to clean it manually
				 */
				if (y == GAME_HEIGHT-1)
				{
					for(int k = 0; k < GAME_WIDTH; k++)
						game_buffer[y][k] = ' ';
				}
				/* Move the game buffer 'down'
				*/
				memmove(&game_buffer[1], &game_buffer, (y) * GAME_WIDTH * sizeof(int));
			}
		}
	}
}

/*
 * Lower current piece by 1
 * Always called by the main game loop
 * Ensures that speed is respected
 * int fid : id of the queue used to simulate a lock (on current_piece)
 */
void lower_current_piece(int fid, int no_wait) {
	if (loose == 1) return;
	if (no_wait || last_time + speed < current_clock())
	{
		psend(fid, 1);
		last_time = current_clock();
		int change_piece = 0;
		for_each_part(current_piece)
		{
			int *px = &current_piece->parts[i].x;
			int *py = &current_piece->parts[i].y;
			if (*py < GAME_HEIGHT-1)
				*py += 1;
			/* Check if the piece touches the floor and change it
			 */
			if (*py == GAME_HEIGHT-1 || game_buffer[*py+1][*px] != ' ')
				change_piece = 1;
		}
		if (change_piece)
		{
			/* Fill the game array with piece parts
			 */
			for_each_part(current_piece)
			{
				game_buffer[current_piece->parts[i].y][current_piece->parts[i].x] = 'o';
			}
			/* Check if we filled some lines
			 */
			check_lines();
			change_current_piece();
		} else {
			current_piece->anchor.y++;
		}
		preceive(fid, NULL);
	}
}

void rotate_piece(int fid) {
	psend(fid, 1);
	position_t old_parts[4];
	int restore = 0;
	memcpy(old_parts, current_piece->parts, 4*sizeof(position_t));
	for_each_part(current_piece)
	{
		int x = current_piece->parts[i].x;
		int y = current_piece->parts[i].y;
		x = -(y - current_piece->anchor.y) + x;
		y = (x - current_piece->anchor.x) + y;
		x = -(y - current_piece->anchor.y) + x;
		current_piece->parts[i].x = x;
		current_piece->parts[i].y = y;
		if (y < 0 || y > GAME_HEIGHT-1 || x < 0 || x > GAME_WIDTH-1 || game_buffer[y][x] != ' ')
		{
			restore = 1;
			break;
		}
	}
	if (restore)
		memcpy(current_piece->parts, old_parts, 4*sizeof(position_t));
	preceive(fid, NULL);
}

void init_game() {
	loose = 0;
	quit = 0;
	current_piece = NULL;
	last_time = 0;
	speed = 20;
	game_position.x = 20;
	score = 0;
	
	for(int i = 0; i < GAME_HEIGHT; i++)
		for(int j = 0; j < GAME_WIDTH; j++)
			game_buffer[i][j] = ' ';
}


int keyboard_handler(void *args) {
	int fid = (int) (long)args;
	int key;
	while(1) {
		get_key(&key);
		switch(key) {
			case 68:
				move_piece(fid, -1, 0);
				break;
			case 67:
				move_piece(fid, 1, 0);
				break;
			case 66:
				lower_current_piece(fid, 1);
				break;
			case 65:
				rotate_piece(fid);
				break;
			case 'q':
				quit = 1;
				return 0;
		}
	}
	return 0;
}

int play_music(void *arg) {
	(void) arg;
	while(1) {
		beep(658, 125);
		beep(1320, 500);
		beep(990, 250);
		beep(1056, 250);
		beep(1188, 250);
		beep(1320, 125);
		beep(1188, 125);
		beep(1056, 250);
		beep(990, 250);
		beep(880, 500);
		beep(880, 250);
		beep(1056, 250);
		beep(1320, 500);
		beep(1188, 250);
		beep(1056, 250);
		beep(990, 750);
		beep(1056, 250);
		beep(1188, 500);
		beep(1320, 500);
		beep(1056, 500);
		beep(880, 500);
		beep(880, 500);
		wait_clock(current_clock() + 25);
		beep(1188, 500);
		beep(1408, 250);
		beep(1760, 500);
		beep(1584, 250);
		beep(1408, 250);
		beep(1320, 750);
		beep(1056, 250);
		beep(1320, 500);
		beep(1188, 250);
		beep(1056, 250);
		beep(990, 500);
		beep(990, 250);
		beep(1056, 250);
		beep(1188, 500);
		beep(1320, 500);
		beep(1056, 500);
		beep(880, 500);
		beep(880, 500);
		wait_clock(current_clock() + 50);
		beep(1320, 500);
		beep(990, 250);
		beep(1056, 250);
		beep(1188, 250);
		beep(1320, 125);
		beep(1188, 125);
		beep(1056, 250);
		beep(990, 250);
		beep(880, 500);
		beep(880, 250);
		beep(1056, 250);
		beep(1320, 500);
		beep(1188, 250);
		beep(1056, 250);
		beep(990, 750);
		beep(1056, 250);
		beep(1188, 500);
		beep(1320, 500);
		beep(1056, 500);
		beep(880, 500);
		beep(880, 500);
		wait_clock(current_clock() + 25);
		beep(1188, 500);
		beep(1408, 250);
		beep(1760, 500);
		beep(1584, 250);
		beep(1408, 250);
		beep(1320, 750);
		beep(1056, 250);
		beep(1320, 500);
		beep(1188, 250);
		beep(1056, 250);
		beep(990, 500);
		beep(990, 250);
		beep(1056, 250);
		beep(1188, 500);
		beep(1320, 500);
		beep(1056, 500);
		beep(880, 500);
		beep(880, 500);
		wait_clock(current_clock() + 50);
		beep(660, 1000);
		beep(528, 1000);
		beep(594, 1000);
		beep(495, 1000);
		beep(528, 1000);
		beep(440, 1000);
		beep(419, 1000);
		beep(495, 1000);
		beep(660, 1000);
		beep(528, 1000);
		beep(594, 1000);
		beep(495, 1000);
		beep(528, 500);
		beep(660, 500);
		beep(880, 1000);
		beep(838, 2000);
		beep(660, 1000);
		beep(528, 1000);
		beep(594, 1000);
		beep(495, 1000);
		beep(528, 1000);
		beep(440, 1000);
		beep(419, 1000);
		beep(495, 1000);
		beep(660, 1000);
		beep(528, 1000);
		beep(594, 1000);
		beep(495, 1000);
		beep(528, 500);
		beep(660, 500);
		beep(880, 1000);
		beep(838, 2000);
	}
}

int command_tetris(void *args) {
	(void) args;

	int fid;

	cons_echo(0);
	clear_screen();

	int music_pid = start(play_music, 4096, 130, "tetris_music", NULL);

	display_banner();
	get_key(NULL);
	clear_screen();

	fid = pcreate(2);

	int keyboard_pid = start(keyboard_handler, 4096, 130, "keyboard_handler", (void *)(long)fid);

	init_game();

	change_current_piece();

	while (!loose && !quit) {
		clear_screen();
		display_game(fid);
		if ( !has_loose() )
		{
			lower_current_piece(fid, 0);
			wait_clock(current_clock() + 5);
		}
	}
	kill(music_pid);
	waitpid(music_pid, NULL);
	kill(keyboard_pid);
	waitpid(keyboard_pid, NULL);
	pdelete(fid);
	
	if(current_piece != NULL) mem_free(current_piece, sizeof(game_piece_t));

	if (loose)
		display_loose_screen();
	
	clear_screen();
	cons_echo(1);

	return 0;
}
