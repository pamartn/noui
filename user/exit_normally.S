# cette directive sert a rendre l’etiquette publique
.weak exit_normally
.globl exit_normally
# debut du traitant
exit_normally:
    pushl %eax
    call exit_p
