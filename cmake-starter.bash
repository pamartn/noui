#!/bin/bash

if [ $# = 0 ]; then
    screen -S vm -X at '#' stuff ^C > /dev/null 2>&1
    screen -dmS vm qemu-system-i386 -m 256 -kernel kernel/kernel.bin -s -S
else
    qemu-system-i386 -m 256 -kernel kernel/kernel.bin
fi