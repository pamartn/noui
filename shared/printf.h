//
// Created by metaheavens on 07/06/18.
//

#ifndef PSYS_BASE_PRINTF_H
#define PSYS_BASE_PRINTF_H

int printf(const char *fmt, ...);

#endif //PSYS_BASE_PRINTF_H
