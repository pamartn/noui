#ifndef CR_CONST_H
#define CR_CONST_H
#define NB_COL 80
#define MEM_VIDEO 0xB8000
#define H_VIDEO 25
#define W_VIDEO 80
#define CMD_CUR 0x3D4
#define DATA_CUR 0x3D5
#define DOWN_CMD 0x0F
#define UP_CMD 0x0E
#define INTERUP_TABLE 0x1000
#define INTERUP_CLOCK 32
#define INTERUP_DATE 40
#define CLOCKFREQ 100 // Was 500, changed according to https://ensiwiki.ensimag.fr/index.php?title=Projet_syst%C3%A8me_:_roadmap#Phase_2
#define SCHEDFREQ 50
#define QUARTZ 0x1234DD
#endif
