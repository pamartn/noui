#include "../kernel/cpu.h"
#include "../shared/const.h"
#include "../kernel/segment.h"
#include "ecran.h"
#include "interupt.h"
#include "../shared/sprintf.h"
#include "process.h"
#include "../shared/printf.h"
#include "../shared/stdbool.h"
#include "../shared/string.h"

static int cpt = 0;

long totalsec = 0;
unsigned long totalclock = 0;

static int sec = 0;
static int min = 0;
static int hour = 0;

char date_buffer[50];

void tic_PIT(void) {
    outb(0x20, 0x20);
    sec = sec + cpt / (CLOCKFREQ - 1);

    long beforesec = totalsec;
    //printf("beforesec: %ld\n", beforesec);
    totalsec = totalsec + cpt / (CLOCKFREQ - 1);
    //printf("totalsec: %ld\n",totalsec);

    if(beforesec != totalsec) {
#ifdef DEV
        printf("Time: %ld\n", totalsec);
#endif
    }

    min = min + sec / 60;
    hour = hour + min / 60;
    min = min % 60;
    hour = hour % 24;
    sec = sec % 60;
    cpt = (cpt + 1) % CLOCKFREQ;
    totalclock ++;

    schedule_handler();
    /*
    char buffer[50];
    int size = sprintf(buffer, "%02u:%02u:%02u", hour, min, sec);
    print_top_right(buffer, size);
     */
}

unsigned long current_clock() {
    return totalclock;
}

void init_traitant_IT(void (*traitant)(void), uint32_t id_traitant, int user) {
    uint32_t *inter_table = (uint32_t *) INTERUP_TABLE;
    uint32_t upword;
    uint32_t lowword;
    upword = (KERNEL_CS << 16) | ((uint32_t) traitant & 0x0000FFFF);
    lowword = 0x8E00 | ((uint32_t) traitant & 0xFFFF0000);
    if(user) {
        lowword |= 0x6000;
    }

    inter_table[2 * id_traitant] = upword;
    inter_table[2 * id_traitant + 1] = lowword;
}

/*void init_mask(void)
{
    char flags = inb(0x21);
    outb(flags & 0xFE, 0x21);
}

void init_frequency(void)
{
    outb(0x34, 0x43);
    outb((QUARTZ/CLOCKFREQ) % 256, 0x40);
    outb((QUARTZ/CLOCKFREQ) >> 8, 0x40);
}*/

void regle_frequence_RTC(void) {
    uint32_t d = 15;
    outb(0x8A, 0x70);
    uint32_t v = inb(0x71);
    outb(0x8A, 0x70);
    outb((v & 0xF0) | d, 0x71);
}

void set_signal_type(void) {
    outb(0x8B, 0x70);
    uint32_t v = inb(0x71);
    outb(0x8B, 0x70);
    outb(v | 0x40, 0x71);
}


void affiche_date(void) {
    outb(0x20, 0x20);
    outb(0x20, 0xA0);
    outb(0x8C, 0x70);
    inb(0x71);
    uint8_t sec = get_sec();
    uint8_t hour = get_hour();
    uint8_t min = get_min();


    int size = sprintf(date_buffer, "%02u:%02u:%02u", hour, min, sec);
    //print_top_left(buffer, size);
}

uint8_t get_sec(void) {
    outb(0x80, 0x70);
    int secBCD = inb(0x71);
    return (secBCD >> 4) * 10 + (secBCD & 0xF);
}

uint8_t get_min(void) {
    outb(0x80 | 2, 0x70);
    int secBCD = inb(0x71);
    return (secBCD >> 4) * 10 + (secBCD & 0xF);
}

uint8_t get_hour(void) {
    outb(0x80 | 4, 0x70);
    int secBCD = inb(0x71);
    return (secBCD >> 4) * 10 + (secBCD & 0xF);
}

uint8_t get_day(void) {
    outb(0x80 | 6, 0x70);
    int secBCD = inb(0x71);
    return (secBCD >> 4) * 10 + (secBCD & 0xF);
}

void clock_settings(unsigned long *quartz, unsigned long *ticks) {
    *quartz = QUARTZ;
    *ticks = QUARTZ / CLOCKFREQ;
}

void get_hour_str(char *str, int length) {
    if ((unsigned)length > sizeof(date_buffer))
        length = sizeof(date_buffer);
    memcpy(str, date_buffer, length);
}
