#include "../shared/queue.h"
#include "debugger.h"
#include "cpu.h"
#include "ecran.h"
#include "interupt.h"
#include "../shared/const.h"
#include "printf.h"
#include "start.h"
#include "process.h"
#include "stddef.h"
#include "../shared/stddef.h"
#include "test.h"
#include "../shared/stdbool.h"

extern char first_stack[FIRST_STACK_SIZE];

extern long totalsec;
extern unsigned long totalclock;

int idle(void *args) {
    (void) args;

    for (;;) {
        sti();
        hlt();
        cli();
    }
    return 1;
}

void kernel_start(void) {
    efface_ecran();
    init_traitant_IT(traitant_IT_32, INTERUP_CLOCK, false);
    set_signal_type();
    regle_frequence_RTC();
    init_mask();
    init_frequency();
    init_traitant_IT(traitant_IT_40, INTERUP_DATE, false);
    init_traitant_IT(traitant_IT_33, 33, false);

    init_traitant_IT(traitant_IT_49, 49, true);

    //Init of process idle
    process_init(0, "idle", 1);

    start(user_start + 6, 4096, 127, "user", NULL);

    idle(NULL);
}
