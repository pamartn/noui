#include "messages.h"
#include "../shared/queue.h"
#include "../shared/stdbool.h"
#include "../shared/stddef.h"
#include "mem.h"
#include "process.h"

#ifndef NBQUEUE
#define NBQUEUE 1024
#endif

#ifndef QUEUE_MAX_SIZE
#define QUEUE_MAX_SIZE 1024
#endif

extern process_t *elected;

typedef struct messages {
    int max_msg;
    int nb_msg;
    link *msg_queue;
    link *read_queue;
    int nb_read;
    link *write_queue;
    int nb_write;
} messages_t;

typedef struct message {
    int val;
    link l;
    int priority;
} message_t;

/**
 * Macro useful to manage messages and process queues that are waiting for messages.
 */

#define message_add(msg, messages) \
do { \
    queue_add(msg, messages->msg_queue, message_t, l, priority); \
    messages->nb_msg++; \
    assert(messages->nb_msg <= messages->max_msg); \
} while(0)

message_t *message_pop(messages_t *messages) {
    message_t *msg = queue_out(messages->msg_queue, message_t, l);
    messages->nb_msg--;
    assert(messages->nb_msg >= 0);
    return msg;
}

#define message_top(messages) queue_top(messages->msg_queue, message_t, l)

#define waiting_read_add(ptr, messages) \
do { \
    queue_add(ptr, messages->read_queue, process_t, wait_read, priority); \
    messages->nb_read++; \
} while(0)

#define waiting_read_top(messages) queue_top(messages->read_queue, process_t, wait_read)
process_t *waiting_read_pop(messages_t *messages) {
    process_t *w = queue_out(messages->read_queue, process_t, wait_read);
    messages->nb_read--;
    return w;
}

process_t *waiting_read_older_pop(messages_t *messages) {
    process_t *actual_older = NULL;
    process_t *actual = NULL;
    queue_for_each(actual, messages->read_queue, process_t, wait_read) {
        if(actual_older == NULL || actual->priority >= actual_older->priority) actual_older = actual;
    }
    queue_del(actual_older, wait_read);
    messages->nb_read--;
    return actual_older;
}

#define waiting_write_add(ptr, messages) \
do { \
    queue_add(ptr, messages->write_queue, process_t, wait_write, priority); \
    messages->nb_write++; \
} while(0)

#define waiting_write_top(messages) queue_top(messages->write_queue, process_t, wait_write)

process_t *waiting_write_pop(messages_t *messages) {
    process_t *w = queue_out(messages->write_queue, process_t, wait_write);
    messages->nb_write--;
    return w;
}

process_t *waiting_write_older_pop(messages_t *messages) {
    process_t *actual_older = NULL;
    process_t *actual = NULL;
    queue_for_each(actual, messages->write_queue, process_t, wait_write) {
        if(actual_older == NULL || actual->priority >= actual_older->priority) actual_older = actual;
    }
    queue_del(actual_older, wait_write);
    messages->nb_write--;
    return actual_older;
}

#define LIST_HEAD_MALLOC(list_head) \
do { \
    list_head = mem_alloc(sizeof(link)); \
    list_head->next = list_head; \
    list_head->prev = list_head; \
} while(0)

/**
 * End of macros
 */

/*
 * All queues are store there
 */
messages_t *queues[NBQUEUE];

int pcreate_fid(int count, int fid);

int pcreate(int count) {
    if (count <= 0 || count >= QUEUE_MAX_SIZE) return -1;
    int i = 0;
    do {
        if (queues[i] == NULL) {
            return pcreate_fid(count, i);
        }
        i++;
    } while (i < NBQUEUE);

    return -1;
}

int pcreate_fid(int count, int fid) {
    queues[fid] = mem_alloc(sizeof(messages_t));
    queues[fid]->max_msg = count;
    queues[fid]->nb_msg = 0;
    queues[fid]->nb_read = 0;
    queues[fid]->nb_write = 0;
    LIST_HEAD_MALLOC(queues[fid]->msg_queue);
    LIST_HEAD_MALLOC(queues[fid]->read_queue);
    LIST_HEAD_MALLOC(queues[fid]->write_queue);

    return fid;
}

process_t *get_elected_waiter() {
    elected->state = WAIT_MSG;
    return elected;
}

message_t *create_message(int msg) {
    message_t *m = mem_alloc(sizeof(message_t));
    m->priority = 0;
    m->val = msg;
    return m;
}

int psend(int fid, int message) {
    if (fid < 0 || fid >= NBQUEUE || queues[fid] == NULL) return -1;
    messages_t *debug = queues[fid]; //TODO REMOVE

    while (queues[fid]->nb_msg == queues[fid]->max_msg) { // Queue full
        // process waiting write
        process_t *waiter = get_elected_waiter();
        waiting_write_add(waiter, queues[fid]);
        messages_t *queue_save = queues[fid];
        schedule();
        // Test if preset and/or pdelete have been called while we were in WAIT_MSG state
        if (queues[fid] == NULL || queues[fid] != queue_save) return -1;
    }
    message_add(create_message(message), queues[fid]);

    // Wake process that is waiting for message
    if (waiting_read_top(queues[fid]) != NULL) {
        process_t *waiter = waiting_read_older_pop(queues[fid]);
        waiter->state = READY;
        switch_process(waiter);
    }
    schedule();
    return 0;
}

int preceive(int fid, int *msg) {
    if(msg != NULL && msg < (int *) 0x1000000) return -1;
    if (fid < 0 || fid >= NBQUEUE || queues[fid] == NULL) return -1;

    while (queues[fid]->nb_msg == 0) { // Queue empty
        // process waiting write
        process_t *waiter = get_elected_waiter();
        waiting_read_add(waiter, queues[fid]);
        messages_t *queue_save = queues[fid];
        schedule();
        // Test if preset and/or pdelete have been called while we were in WAIT_MSG state
        if (queues[fid] == NULL || queues[fid] != queue_save) return -1;
    }

    message_t *out_msg = message_pop(queues[fid]);

    // Wake process that is waiting for writing a message
    if (waiting_write_top(queues[fid]) != NULL) {
        process_t *waiter = waiting_write_older_pop(queues[fid]);
        waiter->state = READY;
        switch_process(waiter);
    }
    if (msg != NULL)
        *msg = out_msg->val;
    mem_free(out_msg, sizeof(message_t));

    schedule();
    return 0;
}

void clean_waiter(int fid) {
    process_t *p = waiting_read_pop(queues[fid]);
    while (p != NULL) {
        assert(p->state == WAIT_MSG);
        p->state = READY;
        p = waiting_read_pop(queues[fid]);
    }

    p = waiting_write_pop(queues[fid]);
    while (p != NULL) {
        assert(p->state == WAIT_MSG);
        p->state = READY;
        p = waiting_write_pop(queues[fid]);
    }
}

void clean_messages(int fid) {
    message_t *m = NULL;

    while (queues[fid]->nb_msg > 0) {
        m = message_pop(queues[fid]);
        mem_free(m, sizeof(message_t));
    }
}

int preset(int fid) {
    if (fid < 0 || fid >= NBQUEUE || queues[fid] == NULL) return -1;

    clean_waiter(fid);
    clean_messages(fid);

    /*
     * We do create the new queue before freeing the new one to ensure that their ptr will
     * not be the same. Usefull in pcreate/preceive to know if the queue has been altered by
     * a preset/pdelete call
     */
    messages_t *old_queue = queues[fid];
    pcreate_fid(queues[fid]->max_msg, fid);

    mem_free(old_queue->msg_queue, sizeof(link));
    mem_free(old_queue->write_queue, sizeof(link));
    mem_free(old_queue->read_queue, sizeof(link));
    mem_free(old_queue, sizeof(messages_t));

    schedule();
    return 0;
}

int pdelete(int fid) {
    if (fid < 0 || fid >= NBQUEUE || queues[fid] == NULL) return -1;

    messages_t *debug = queues[fid]; //TODO REMOVE

    clean_waiter(fid);
    clean_messages(fid);

    mem_free(queues[fid]->msg_queue, sizeof(link));
    mem_free(queues[fid]->write_queue, sizeof(link));
    mem_free(queues[fid]->read_queue, sizeof(link));
    mem_free(queues[fid], sizeof(messages_t));

    queues[fid] = NULL;

    schedule();
    return 0;
}

int pcount(int fid, int *count) {
    if(count != NULL && count < (int *) 0x1000000) return -1;
    if (fid < 0 || fid >= NBQUEUE || queues[fid] == NULL) return -1;

    if (queues[fid]->nb_msg > 0) {
        if(count != NULL) *count = queues[fid]->nb_msg + queues[fid]->nb_write;
    } else {
        if(count != NULL) *count = -queues[fid]->nb_read;
    }

    return 0;
}

/**
 * Allows you to display queues messages and process that are waiting.
 */
void msgstate() {
    for(int i = 0; i < NBQUEUE; i++) {
        if(queues[i] != NULL) {
            printf("Queue 1:\n");
            printf("   Messages:\n");
            message_t* actual_msg = NULL;
            queue_for_each(actual_msg, queues[i]->msg_queue, message_t, l) {
                printf("      - %d\n", actual_msg->val);
            }

            process_t* actual_proc = NULL;
            printf("   Read queue:\n");
            queue_for_each(actual_proc, queues[i]->read_queue, process_t, wait_read) {
                printf("      - %d: %s\n", actual_proc->pid, actual_proc->pname);
            }

            printf("   Write queue:\n");
            queue_for_each(actual_proc, queues[i]->write_queue, process_t, wait_write) {
                printf("      - %d: %s\n", actual_proc->pid, actual_proc->pname);
            }
        }
    }
}