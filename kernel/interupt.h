#ifndef _INTERUPT_H_
#define _INTERUPT_H_

#include "stdint.h"
#include "../shared/stdint.h"

extern long totalsec;
extern unsigned long totalclock;
extern char date_buffer[];

extern void init_traitant_IT(void (* traitant)(void), uint32_t id_traitant, int user);
extern void init_mask(void);
extern void traitant_IT_32(void);
extern void traitant_IT_33(void);
extern void traitant_IT_40(void);
extern void traitant_IT_49(void);
extern void init_frequency(void);
extern void regle_frequence_RTC(void);
extern void set_signal_type(void);
extern void affiche_date(void);
extern uint8_t get_sec(void);
extern uint8_t get_min(void);
extern uint8_t get_hour(void);
extern uint8_t get_day(void);
void clock_settings(unsigned long *quartz, unsigned long *ticks);
unsigned long current_clock();
void get_hour_str(char *str, int length);

#endif