#ifndef _SOUND_H_
#define _SOUND_H_
#include "stdint.h"

void beep(uint32_t frequency, uint32_t duration);

#endif
