#include "../shared/stddef.h"
#include "../shared/stdint.h"
#include "../shared/console.h"
#include "../shared/printf.h"
#include "ecran.h"
#include "process.h"
#include "messages.h"
#include "interupt.h"
#include "sound.h"


int32_t system_call(uint32_t call_id, void *arg1, void *arg2, void *arg3, void *arg4, void *arg5) {
    //printf("Args: %d %x %x %x %x %x\n", (int) call_id, (int) arg1, (int) arg2, (int) arg3, (int) arg4, (int) arg5);

    switch (call_id) {
        case 0:
        case 1:
            if(arg1 < (void *) 0x1000000) return -1;
            console_putbytes((char *) arg1, (int) arg2);
            break;
        case 2:
            cons_read((char *) arg1, (unsigned long) arg2);
            break;
        case 3:
            exit_p((int) arg1);
            break;
        case 4:
            return start_user((int (*)(void *)) arg1, (unsigned long) arg2, (int) arg3, (const char *) arg4, (void *) arg5);
        case 5:
            return waitpid((int) arg1, (int *) arg2);
        case 6:
            return kill((int) arg1);
        case 7:
            return getpid();
        case 8:
            return getprio((int) arg1);
        case 9:
            return chprio((int) arg1, (int) arg2);
        case 10:
            printf("Not implemented, lol\n");
            break;
        case 11:
            return pcreate((int) arg1);
        case 12:
            return psend((int) arg1, (int) arg2);
        case 13:
            return pdelete((int) arg1);
        case 14:
            return pcount((int) arg1, (int *) arg2);
        case 15:
            return preceive((int) arg1, (int *) arg2);
        case 16:
            return preset((int) arg1);
        case 17:
            clock_settings(arg1, arg2);
            break;
        case 18:
            wait_clock((unsigned long) arg1);
            break;
        case 19:
            return (int32_t) current_clock();
        case 20:
            cons_echo((int) arg1);
            break;
            /*
             * Custom system calls
             */
        case 21:
            get_hour_str(arg1, (int) arg2);
            break;
        case 22:
			top();
            break;
        case 23:
			ps();
            break;
		case 24:
			ecrit_car((uint32_t) arg1, (uint32_t) arg2, (char) (int) arg3, (uint32_t) arg4, (uint32_t) arg5);
			break;
		case 25:
			efface_ecran();
			break;
		case 26:
			get_key((int *) arg1);
			break;
        case 27:
            msgstate();
            break;
		case 28:
			beep((uint32_t) arg1, (uint32_t) arg2);
			break;
        default:
            printf("ERROR unknown syscall\n");
            break;
    }
}
