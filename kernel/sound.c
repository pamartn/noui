#include "sound.h"
#include "cpu.h"
#include "process.h"
#include "interupt.h"

//Play sound using built in speaker
static void start_sound(uint32_t frequency) {
	uint32_t Div;
	uint8_t tmp;

	//Set the PIT to the desired frequency
	Div = 1193180 / frequency;
	/*
	outb(0x43, 0xb6);
	outb(0x42, (uint8_t) (Div) );
	outb(0x42, (uint8_t) (Div >> 8));
	*/
	outb(0xb6, 0x43);
	outb((uint8_t) (Div), 0x42);
	outb((uint8_t) (Div >> 8), 0x42);

	//And play the sound using the PC speaker
	tmp = inb(0x61);
	if (tmp != (tmp | 3)) {
		outb(tmp | 3, 0x61);
	}
}


//make it shutup
static void stop_sound() {
	uint8_t tmp = inb(0x61) & 0xFC;
	outb(tmp, 0x61);
}

void beep(uint32_t frequency, uint32_t duration) {
	start_sound(frequency);
	wait_clock(current_clock() + duration/10);
	stop_sound();
}
